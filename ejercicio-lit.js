import { LitElement, html, css } from 'lit-element';
import '../src/ejercicio-tabla.js'

export class EjercicioLit extends LitElement {
	static get styles() {
		return css`
		`;
	}

	static get properties(){
		return {
			cadena: { type: String },
            arreglo: { type: Array }
		}
	}

	constructor(){
		super();
        this.cadena = '';
        this.arreglo = [];
	}

    buscar(e){
		e.preventDefault();
        console.log(this.shadowRoot.querySelector("#inputBusca"));
		this.cadena = this.shadowRoot.querySelector("#inputBusca").value;

        fetch('http://dummy.restapiexample.com/api/v1/employees')
        .then(response => response.json())
        .then(datas => { this.arreglo = datas.data });

        this.shadowRoot.querySelector("ejercicio-tabla").cadena=this.cadena;
        this.shadowRoot.querySelector("ejercicio-tabla").arreglo = this.arreglo;
        this.shadowRoot.querySelector("ejercicio-tabla").render;
	}

    limpiar(e){
		e.preventDefault();
		this.cadena = '';
        this.shadowRoot.querySelector("ejercicio-tabla").cadena=this.cadena;
        this.shadowRoot.querySelector("ejercicio-tabla").render();
	}

	render() {
		return html`
			<div>
                <label for="inputBusca">Search</label>
                <input id="inputBusca" name="inputBusca">
                <button id="find" @click="${this.buscar}">Find</button>
                <button id="clear" @click="${this.limpiar}">Clear</button>
                <br>
                <ejercicio-tabla
                    .cadena="${this.cadena}"
                    .arreglo="${this.arreglo}"
                ></ejercicio-tabla>
			</div>
		`;
	}
}

customElements.define('ejercicio-lit', EjercicioLit);