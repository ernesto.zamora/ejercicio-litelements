import { LitElement, html, css } from 'lit-element';

export class EjercicioTabla extends LitElement {
	static get styles() {
		return css`
		`;
	}

	static get properties(){
		return {
			cadena: { type: String },
            arreglo: { type: Array }
		}
	}

	constructor(){
		super();
		this.cadena = '';
        this.arreglo = [];
	}

	render() {
		return html`
		<table>
            ${this.arreglo.map(empleado => html`
                ${empleado.employee_name.includes(this.cadena) ? html`
                    <tr>
                        <td>${empleado.id}"</td>
                        <td>${empleado.employee_name}"</td>
                    </tr>
                ` : '' }
            `)}
		</table>
		`;
	}
}

customElements.define('ejercicio-tabla', EjercicioTabla);